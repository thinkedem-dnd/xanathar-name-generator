import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { ElementTypes } from '../constants';

import Dialog from 'material-ui/Dialog';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import { getChipDraft, getChipValidation, getTableList, isEditing } from "../reducers";
import { closeChipEditor, saveChip, updateChipDraft } from "../actions/formatActions";

class FormatForm extends Component {
    constructor() {
        super(...arguments);
        this.onTableSelect = this.onTableSelect.bind(this);
        this.onEdit = this.onEdit.bind(this);
        this.onSelectType = this.onSelectType.bind(this);
    }

    render() {
        let fields = [];
        switch (this.props.draft.type) {
            case ElementTypes.CONSTANT:
                fields.push(
                    <TextField
                        key="code"
                        name="code"
                        floatingLabelText="Value"
                        value={this.props.draft.code}
                        onChange={this.onEdit}
                        errorText={this.props.errors.codeError}
                        fullWidth={true}
                    />
                );
                break;

            case ElementTypes.TABLE:
                const table = (this.props.draft.table ? this.props.draft.table.code : null);
                fields.push(
                    <SelectField
                        key="table"
                        name="table"
                        floatingLabelText="Table"
                        value={table}
                        onChange={this.onTableSelect}
                        fullWidth={true}
                    >
                        {this.props.tables.map(table => <MenuItem key={table.code} value={table.code} primaryText={table.name} />)}
                    </SelectField>
                );
                fields.push(
                    <TextField
                        key="prob"
                        name="prob"
                        floatingLabelText="Probability"
                        onChange={this.onEdit}
                        value={this.props.draft.prob}
                        errorText={this.props.errors.probError}
                    />
                );
                break;
            default: break;
        }

        const actions = [
            <FlatButton label="Cancel" onClick={this.props.closeChipEditor} />,
            <FlatButton
                label="Save"
                onClick={() => this.props.saveChip(this.props.draft)}
                primary={true}
                disabled={this.props.invalid}
            />
        ];
        
        return (
            <Dialog 
                title="Format edit"
                modal={true}
                open={this.props.open}
                onRequestClose={this.props.closeChipEditor}
                actions={actions}
            >
                <SelectField
                    name="type"
                    value={this.props.draft.type}
                    floatingLabelText="Type"
                    onChange={this.onSelectType}
                    fullWidth={true}
                >
                    <MenuItem id={ElementTypes.CONSTANT} value={ElementTypes.CONSTANT} primaryText="Fix text" />
                    <MenuItem id={ElementTypes.TABLE} value={ElementTypes.TABLE} primaryText="Roll on table" />
                </SelectField>
                {fields}
                
            </Dialog>
        );
    }

    onEdit({target}, value) {
        if (!!target && !!target.name) {
            this.props.updateChipDraft(target.name, value);
        }
    }

    onSelectType({target}, index, value) {
        this.props.updateChipDraft('type', value);
    }

    onTableSelect({target}, index, value) {
        if (!this.props.draft.table || value !== this.props.draft.table.code) {
            const table = this.props.tables.find(t => t.code === value);
            this.props.updateChipDraft('table', table);
        }
    }

}

FormatForm.propTypes = {
    open:           PropTypes.bool,
    draft:           PropTypes.shape({
        id:     PropTypes.number,
        type:   PropTypes.string.isRequired,
        code:   PropTypes.string,
        prob:   PropTypes.oneOfType([PropTypes.number, PropTypes.string])
    }),
    tables:  PropTypes.arrayOf(
        PropTypes.shape({
            code:   PropTypes.string.isRequired,
            name:   PropTypes.string.isRequired
        })
    ).isRequired,
};

const mapStateToProps = (state) => {
    const errors = getChipValidation(state);
    return {
        open:   isEditing(state),
        draft:   getChipDraft(state),
        errors,
        invalid: (Object.keys(errors).length > 0),
        tables: getTableList(state)
    };
};

const mapDispatchToProps = {
    closeChipEditor,
    updateChipDraft,
    saveChip
};

FormatForm = connect(mapStateToProps, mapDispatchToProps)(FormatForm);
export default FormatForm;
