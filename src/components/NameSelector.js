import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import { CUSTOM_FORMULA_ID } from '../constants';
import * as actions from '../actions/nameActions';
import { getFormulaList } from "../reducers";

class NameSelector extends Component {

    constructor() {
        super(...arguments);
        this.onSelect = this.onSelect.bind(this);
    }
    componentDidMount() {
        if (!this.props.id) {
            this.props.setFormula('', CUSTOM_FORMULA_ID);
        }
    }
    
    render() {
        const formatList = this.props.collections.map(collection => (<MenuItem key={collection.id} value={collection.id} primaryText={collection.name} />));
        
        return (
            <SelectField 
                name="collection"
                value={this.props.id}
                floatingLabelText="Name format"
                onChange={this.onSelect}
                fullWidth={true}
            >
                {formatList}
            </SelectField>
        );
    }

    onSelect({target}, index, id) {
        if (id !== this.props.id) {
            let formula = '{invalid}', name = '{invalid}';
            const collection = this.props.collections.find(c => c.id === id);
            if (!!collection) {
                formula = collection.formula || this.props.formula;
                name = collection.name;
            }
            this.props.setFormula(formula, id, name);
        }
    }
};

NameSelector.propTypes = {
    id:             PropTypes.number,
    formula:        PropTypes.string,
    collections:    PropTypes.arrayOf(
        PropTypes.shape({
            id:         PropTypes.number.isRequired,
            name:       PropTypes.string.isRequired,
            formula:    PropTypes.string.isRequired
        })
    )
};

NameSelector.defaultProps = {
    id:             null,
    formula:        '',
    collections:    []
};

const mapStateToProps = (state) => ({
    ...state.format,
    collections: getFormulaList(state),

});

NameSelector = connect(mapStateToProps, actions)(NameSelector);
export default NameSelector;
