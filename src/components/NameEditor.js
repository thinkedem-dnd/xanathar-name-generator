import React  from 'react';
import { connect } from 'react-redux';
import { Droppable } from 'react-beautiful-dnd';
import NameChip from './NameChip';
import IconButton from 'material-ui/IconButton';
import AddIcon from 'material-ui/svg-icons/content/add-circle';
import { styles } from '../constants';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { getFormulaElements } from "../reducers";
import { editChip, removeChip } from "../actions/formatActions";

let NameEditor = (props) => {
    const chips = props.elements
        .map((item) => (
            <NameChip
                key={item.id}
                item={item}
                onClick={() => props.editChip(item)}
                onRemove={() => props.removeChip(item.id)}
                palette={props.muiTheme.palette}
            />
        ));

    return (
        <Droppable droppableId="nameEditor" direction="horizontal">
            {(provided, snapshot) => (
                <div ref={provided.innerRef} style={styles.chipWrapper}>
                    {chips}
                    <IconButton onClick={() => props.editChip()} >
                        <AddIcon color={props.muiTheme.palette.primary2Color} />
                    </IconButton>
                    {provided.placeholder}
                </div>
            )}
        </Droppable>

    );
};

const mapStateToProps = (state) => ({
    elements: getFormulaElements(state),
});

const mapDispatchToProps = {
    removeChip, editChip
};

NameEditor = connect(mapStateToProps, mapDispatchToProps)(NameEditor);

export default muiThemeable()(NameEditor);
