import React from 'react';
import TitleBar from './TitleBar';
import NameEditorContainer from '../containers/NameEditorContainer';
import NameList from './NameList';
import Copyright from './Copyright';
import Donate from "./Donate";

const Root = () => (
    <div className="App">
        <TitleBar />
        <div className="cards">
            <NameEditorContainer />
            <NameList />
            <Donate />
            <Copyright />
            {(window.location.protocol !== 'https:') && <div>Note: if you only see the "Custom" name format, make sure <a href="https://fantasy-name-generator.herokuapp.com">you use the site via https</a>.</div> }
        </div>
    </div>

);

export default Root;
