import React from 'react';
import { Card, CardText } from 'material-ui/Card';
const Copyright = () => (
    <Card className="card">
        <CardText>
            DUNGEONS & DRAGONS, D&D, Wizards of the Coast, Forgotten Realms, the dragon ampersand, <em>Player's
            Handbook</em>, <em>Monster Manual</em>, <em>Dungeon Master's Guide</em>, <em>Xanathar's Guide to
            Everything</em>, all other Wizards of the Coast product names, and their respective logos are
            trademarks of Wizards of the Coast in the USA and other countries. All characters and their distinctive
            likenesses are property of Wizards of the Coast.
        </CardText>
    </Card>
);

export default Copyright;
