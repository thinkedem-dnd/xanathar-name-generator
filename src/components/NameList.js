import React from 'react';
import { connect } from 'react-redux';
import { Card } from 'material-ui/Card';
import { List } from 'material-ui/List';
import NameItem from './NameItem';

import { getNames } from "../reducers";
import { removeName } from "../actions/nameActions";

let NameList = ({names, removeName}) => {
    if (!names || names.length === 0) { return null; } //guard
    const items = names.map(item => (<NameItem key={item.id} data={item} removeAction={removeName}/>));

    return (
        <Card className="card">
            <List>
                {items}
            </List>
        </Card>
    );
};

const mapStateToProps = (state) => ({
    names: getNames(state)
});

const mapDispatchToProps = (dispatch) => ({
    removeName(id) { dispatch(removeName(id)); }
});

NameList = connect(mapStateToProps, mapDispatchToProps)(NameList);
export default NameList;
