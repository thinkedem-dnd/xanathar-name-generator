import React from 'react';
import PropTypes from 'prop-types';
import { ListItem } from 'material-ui/List';
import IconButton from 'material-ui/IconButton';
import RemoveIcon from 'material-ui/svg-icons/navigation/cancel';

const NameItem = ({data, removeAction}) => {
    const removeButton = (
        <IconButton onClick={() => removeAction(data.id)} >
            <RemoveIcon />
        </IconButton>
    );

    return (
        <ListItem
            primaryText={data.name}
            secondaryText={data.format}
            rightIconButton={removeButton}
        />
    );
};

NameItem.propTypes = {
    data:   PropTypes.shape({
        id:     PropTypes.string.isRequired,
        name:   PropTypes.string.isRequired,
        type:   PropTypes.string
    }),

    removeAction:   PropTypes.func.isRequired
};

export default NameItem;
