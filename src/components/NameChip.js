import React from 'react';
import PropTypes from 'prop-types';
import { Draggable } from 'react-beautiful-dnd';
import Chip from 'material-ui/Chip';
import { styles, ElementTypes } from '../constants';
import muiThemeable from 'material-ui/styles/muiThemeable';
import Avatar from 'material-ui/Avatar';
import DragHandleIcon from 'material-ui/svg-icons/action/reorder';

const NameChip = props => {
    let name = props.item.code;
    let color = props.palette.textColor;
    let backgroundColor = props.palette.primary3Color;

    if (props.item.type === ElementTypes.TABLE) {
        color = props.palette.alternateTextColor;
        if (!props.item.table) {
            name = props.item.code;
            backgroundColor = props.palette.accent1Color;
        } else {
            backgroundColor = props.palette.primary1Color;
            name = props.item.table.name;
            if (!!props.item.prob && 1 > props.item.prob) {
                name += ` | ${Math.floor(props.item.prob * 100)}%`;
            }
        }
    }

    let chipProps = {
        style:              styles.chip,
        onRequestDelete:    props.onRemove,
        onClick:            props.onClick,
        color, backgroundColor
    };

    const avatarProps = {
        style:      styles.dragHandle,
        icon:       <DragHandleIcon/>,
        color, backgroundColor
    };

    return(
        <Draggable draggableId={`drag-${props.item.id}`} >
            {(provided, snapshot) => (
                <div>
                    <div
                        ref={provided.innerRef}
                        style={provided.draggableStyle}
                    >
                        <Chip {...chipProps} >
                            <Avatar {...avatarProps} {...provided.dragHandleProps} />
                            {name}
                        </Chip>
                    </div>
                    {provided.placeholder}
                </div>
            )}
        </Draggable>
    );
};

NameChip.propTypes = {
    item:   PropTypes.shape({
        id:     PropTypes.number.isRequired,
        type:   PropTypes.string.isRequired,
        code:   PropTypes.string.isRequired,
        table:  PropTypes.obj
    }),

    palette:    PropTypes.object.isRequired,
    onClick:    PropTypes.func,
    onRemove:   PropTypes.func
};

export default muiThemeable()(NameChip);
