import React from 'react';
import PropTypes from 'prop-types';
import { AppBar } from 'material-ui';

const TitleBar = props => (
    <AppBar title={props.title} />
);

TitleBar.propTypes = {
    title:      PropTypes.string
};

TitleBar.defaultProps = {
    title:      `Edem's Fantasy Name Generator`
};

export default TitleBar;
