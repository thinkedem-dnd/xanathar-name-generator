import React from "react";
import { connect } from "react-redux";
import { Card, CardTitle, CardText, CardActions } from "material-ui/Card";
import FlatButton from "material-ui/FlatButton";
import {isCardExpanded} from '../reducers';
import {Cards} from '../constants';
import {toggleCard} from '../actions/systemActions';

const Donate = ({cardExpanded = false, toggleCard}) => {

    return (
        <Card
            className="card"
            expanded={cardExpanded}
            onExpandChange={toggleCard}
        >
            <CardTitle actAsExpander={true} >
                Usage of this app will always be free, however you can donate if you wish to.
            </CardTitle>
            <CardActions>
                <FlatButton href='https://ko-fi.com/W7W0AB6G' target='_blank'><img style={{border:0, height:"36px"}} src="https://az743702.vo.msecnd.net/cdn/kofi2.png?v=0" alt='Buy Me a Coffee at ko-fi.com' /></FlatButton>
                <FlatButton label="Why buy me a coffee?" onClick={toggleCard} />
            </CardActions>
            <CardText expandable={true} >
                <p>Yes, it's there for free. You don't need to pay anything for it. However, if you wish to donate to me, you can certainly do through PayPal. But why would you do
                    that, if this is free anyway, you ask?</p>
                <p>First of all, I do have some hosting costs. Not much, but every little would help. Also the frontend part is on a free dyno, which basically means it takes
                    a while to load sometimes. First and foremost I will use the donation to upgrade the hosting for better availability.
                    Second, I still have plans for this. You might have noticed, it only contains a limited list of names, and there's definitely that one thing you wish the app
                    would do. However, I am using my free time for development, donations would mean I focus more of that free time in adding new features. Planned features include:</p>
                <ul>
                    <li>Bit more intuitive interface</li>
                    <li>Exporting generated lists in txt or csv format (or at least copy to clipboard)</li>
                    <li>Login using Google, Facebook or just an email and password to personalize content, like:
                        <ul>
                            <li>Adding / editing name lists</li>
                            <li>Creating name sets</li>
                            <li>Store list of generated names</li>
                            <li>Add notes to stored names (ad-hoc NPC notes)</li>
                        </ul>
                    </li>
                </ul>
                <p>I chose to use Ko-Fi because it was easy to set up, and deep down it's just a fancy front for PayPal.</p>
            </CardText>
            <CardActions expandable={true}>
                <FlatButton label="Gotcha!" onClick={toggleCard} />
            </CardActions>
        </Card>
    );
};

const mapStateToProps = (state) => ({
    cardExpanded: isCardExpanded(state, Cards.DONATE_CARD)
});

const mapDispatchToProps = (dispatch) => ({
    toggleCard: () => dispatch(toggleCard(Cards.DONATE_CARD))
});

export default connect(mapStateToProps, mapDispatchToProps)(Donate);
