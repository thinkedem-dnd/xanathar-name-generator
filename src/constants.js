export const ActionTypes = {
    NAME_GENERATE:          'name_generate',
    NAME_ADD:               'name_add',
    NAME_REMOVE:            'name_remove',
    NAME_CLEAR:             'name_clear',

    CHIP_EDIT:              'chip_edit',
    CHIP_SAVE:              'chip_save',
    CHIP_MOVE:              'chip_move',
    CHIP_REMOVE:            'format_remove',

    CHIP_CREATE_DRAFT:      'format_create_draft',
    CHIP_UPDATE_DRAFT:      'format_update_draft',
    CHIP_CLEAR_DRAFT:       'format_clear_draft',

    FORMAT_SET:             'format_set',

    FORMULA_LIST_REQUEST:   'formula_list_request',
    FORMULA_LIST_SUCCESS:   'formula_list_success',
    FORMULA_LIST_FAILURE:   'formula_list_failure',

    TABLE_LIST_REQUEST:     'table_list_request',
    TABLE_LIST_SUCCESS:     'table_list_success',
    TABLE_LIST_FAILURE:     'table_list_failure',

    DIALOG_OPEN:            'dialog_open',
    DIALOG_CLOSE:           'dialog_close',

    TOGGLE_CARD:            'toggle_card',

};

export const CUSTOM_FORMULA_ID = -1;

export const ElementTypes = {
    CONSTANT:   'constant',
    TABLE:      'table'
};

export const DialogTypes = {
    FORMAT_DRAFT:           'formatDraft'
};

export const Cards = {
    DONATE_CARD: 'donate_card'
};

export const styles = {
    chip: {
        margin: 4
    },
    dragHandle:  {
        cursor: 'pointer'
    },
    chipWrapper: {
        padding: '16px 0',
        display: 'flex',
        flexWrap: 'wrap'
    }
};
