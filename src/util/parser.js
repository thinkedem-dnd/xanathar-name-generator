import { ElementTypes } from '../constants';

const formatStringPattern = /^\{[\w-]*(\?(\d+(\.\d+)?)?)?}$/;

const codeParser = (tables) => (code, id) => {
    if (formatStringPattern.test(code)) {
        const pattern = code.slice(1, -1).split('?');
        const table = tables.find(table => table.code === pattern[0]);

        return {
            id,
            code,
            type:   ElementTypes.TABLE,
            table,
            prob:   (pattern.length > 1 ? (Number(pattern[1]) || 0.5) : 1)
        };

    } else {
        return {
            id,
            code,
            type:   ElementTypes.CONSTANT
        };
    }
};

export const generateCode = (format) => {
    switch (format.type) {
        case ElementTypes.TABLE:
            let prob = '';
            if (format.prob === 0.5) {
                prob = '?';
            } else if (format.prob < 1) {
                prob = `?${format.prob}`;
            }
            return `{${format.table.code}${prob}}`;

        case ElementTypes.CONSTANT:
        default:
            return format.code;
    }
};

export const getFormulaParser = (tables) => (formula) => {
    const parser = codeParser(tables);
    return formula.trim().split("|").filter(code => !!code).map(parser);
};

export const generateFormula = (elements) => elements.map(item => item.code).join("|");

export const generateName = (elements) => {
    let name = [];
    elements.forEach(part => {
        if (!!part.prob && part.prob < 1 && Math.random() >= part.prob) { return; } // guard: optional element
        switch (part.type) {
            case ElementTypes.TABLE:
                if (!!part.table) {
                    name.push( part.table.list[ Math.floor(Math.random() * part.table.list.length) ] );
                }
                break;
            default:
                name.push(part.code);
        }
    });
    return name.join(' ') || "«empty name»";
};
