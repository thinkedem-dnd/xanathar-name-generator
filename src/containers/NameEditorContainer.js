import React, { Component } from 'react';
import { connect } from 'react-redux';
import { DragDropContext } from 'react-beautiful-dnd';
// import { DialogTypes } from '../constants';

import { Card, CardActions, CardText } from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import LinearProgress from 'material-ui/LinearProgress';

import NameEditor from '../components/NameEditor';
import NameSelector from '../components/NameSelector';
import FormatForm from '../components/FormatForm';
import { isFetching, getFormula } from '../reducers';
import { clearNames, generateName } from "../actions/nameActions";
import { getTables } from "../actions/tableActions";
import { getFormulas } from "../actions/formulaActions";
import { moveChip } from "../actions/formatActions";

class NameEditorContainer extends Component {

    constructor() {
        super(...arguments);
        this.state = {
            editDialog: false
        };
        this.onDragEnd = this.onDragEnd.bind(this);
    }

    componentDidMount() {
        this.props.getTables();
        this.props.getFormulas();
    }

    render() {
        const progress = (this.props.isFetching ? <LinearProgress /> : null);
        return (
            <Card className="card">
                <CardText>
                    <NameSelector />
                    <DragDropContext onDragEnd={this.onDragEnd} onDragStart={this.onDragEnd} >
                        <NameEditor />
                    </DragDropContext>
                    <FormatForm />
                </CardText>
                <CardActions>
                    <FlatButton label="clear" onClick={this.props.clearNames} />
                    <FlatButton label="Generate" onClick={this.props.generateName} disabled={!this.props.formula} primary={true} />
                </CardActions>
                {progress}
            </Card>
        );
    }

    onDragEnd({source, destination}) {
        if (!destination) { return; } // guard: dropped outside
        this.props.moveChip(source.index, destination.index);
    }
};

const mapStateToProps = (state) => ({
    formula: getFormula(state),

    isFetching: isFetching(state)
});

const mapDispatchToProps = {
    getTables,
    getFormulas,
    generateName,
    clearNames,
    moveChip
};

NameEditorContainer = connect(mapStateToProps, mapDispatchToProps)(NameEditorContainer);
export default NameEditorContainer;
