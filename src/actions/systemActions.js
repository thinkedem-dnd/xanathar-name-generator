import {ActionTypes} from '../constants';

export const toggleCard = (card) => ({ type: ActionTypes.TOGGLE_CARD, card});
