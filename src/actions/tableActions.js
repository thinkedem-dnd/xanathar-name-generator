import { normalize } from 'normalizr';
import { ActionTypes } from '../constants';
import { isFetchingTable } from '../reducers';
import * as schema from './schema';
import * as api from '../api';

export const getTables = () => (dispatch, getState) => {
    if (isFetchingTable(getState())) { return Promise.resolve(); }

    dispatch({ type: ActionTypes.TABLE_LIST_REQUEST });

    return api.fetchTables().then(
        response => {
            dispatch({
                type: ActionTypes.TABLE_LIST_SUCCESS,
                response: normalize(response, schema.arrayOfTables)
            });
        },
        error => {
            dispatch({
                type: ActionTypes.TABLE_LIST_FAILURE,
                message: error.message || 'Something went wrong'
            })
        }
    );
};
