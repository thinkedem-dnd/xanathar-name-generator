import { schema } from 'normalizr';

export const table = new schema.Entity('table', {}, {idAttribute: 'code'});
export const arrayOfTables = [ table ];

export const formula = new schema.Entity('formula');
export const arrayOfFormula = [ formula ];

export const name = new schema.Entity('name');
export const arrayOfNames = [ name ];
