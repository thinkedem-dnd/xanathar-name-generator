import { normalize } from 'normalizr';
import { ActionTypes, CUSTOM_FORMULA_ID } from '../constants';
import * as parser from "../util/parser";
import { getFormatName, getFormulaElements, getTableList } from "../reducers";
import * as schema from './schema';
import { v4 } from 'uuid';

export const setFormula = (formula = '', id = CUSTOM_FORMULA_ID, name) => (dispatch, getState) => {
    const tables = getTableList(getState());
    const elements = parser.getFormulaParser(tables)(formula);

    dispatch({
        type: ActionTypes.FORMAT_SET,
        data: {formula, id, name, elements}
    });
};

export const generateName = () => (dispatch, getState) => {
    const state = getState();
    const elements = getFormulaElements(state);
    const element = {
        id:   v4(),
        name: parser.generateName(elements),
        format: getFormatName(state)
    };

    dispatch({
        type: ActionTypes.NAME_ADD,
        response: normalize(element, schema.name)
    });
};

export const removeName = (id) => ({ type: ActionTypes.NAME_REMOVE, data: {id}});
export const clearNames = () => ({type: ActionTypes.NAME_CLEAR});
