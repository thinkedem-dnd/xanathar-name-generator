import { normalize } from 'normalizr';
import { ActionTypes } from '../constants';
import { isFetchingFormula } from '../reducers';
import * as schema from './schema';
import * as api from '../api';

export const getFormulas = () => (dispatch, getState) => {
    if (isFetchingFormula(getState())) { return Promise.resolve(); }

    dispatch({ type: ActionTypes.FORMULA_LIST_REQUEST });

    return api.fetchFormulas().then(
        response => {
            dispatch({
                type: ActionTypes.FORMULA_LIST_SUCCESS,
                response: normalize(response, schema.arrayOfFormula)
            });
        },
        error => {
            dispatch({
                type: ActionTypes.FORMULA_LIST_FAILURE,
                message: error.message || 'Something went wrong'
            })
        }
    );
};
