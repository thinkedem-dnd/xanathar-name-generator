import { ActionTypes } from "../constants";

export const moveChip = (chipIndex, afterIndex) => ({type: ActionTypes.CHIP_MOVE, chipIndex, afterIndex});
export const removeChip = (chipId) => ({type: ActionTypes.CHIP_REMOVE, chipId});
export const editChip = (draft) => ({type: ActionTypes.CHIP_EDIT, isEditing: true, draft});
export const saveChip = (draft) => ({type: ActionTypes.CHIP_SAVE, draft});

export const updateChipDraft = (field, value) => ({type: ActionTypes.CHIP_UPDATE_DRAFT, field, value });
export const closeChipEditor = () => ({type: ActionTypes.CHIP_EDIT, isEditing: false});
