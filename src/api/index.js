import Api from './ApiRequest';

export const fetchTables =  () => Api.get('namelists');
export const fetchFormulas = () => Api.get('formulas');
