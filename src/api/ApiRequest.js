import 'whatwg-fetch';
const API_URL = process.env.REACT_APP_BACKEND_API;
const API_HEADERS = {
    'Content-Type': 'application/json',
};

let ApiRequest = {
    post(url, params) {
        return fetch(`${API_URL}/${url}`, {
            headers:    API_HEADERS,
            method:     'post',
            body:       JSON.stringify(params)
        })
            .then((response) => response.json());
    },

    get(url) {
        return fetch(`${API_URL}/${url}`, {headers: API_HEADERS})
            .then((response) => response.json());
    },

    put(url, params) {
        return fetch(`${API_URL}/${url}`, {
            headers:    API_HEADERS,
            method:     'put',
            body:       JSON.stringify(params)
        })
            .then((response) => response.json());
    },

    delete(url) {
        return fetch(`${API_URL}/${url}`, {
            headers:    API_HEADERS,
            method:     'delete',
        });
    }


};

export default ApiRequest;
