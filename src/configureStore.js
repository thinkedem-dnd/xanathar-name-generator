import { createStore, applyMiddleware } from 'redux';
import NameGeneratorStore, { getPersistedState } from './reducers';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import throttle from "lodash/throttle";
import { loadState, saveState } from "./localStorage";


const persistedState = loadState();

const configureStore = () => {
    const middlewares = [thunk];
    if (process.env.NODE_ENV !== 'production') {
        middlewares.push(logger);
    }

    const store = createStore(
        NameGeneratorStore,
        persistedState,
        applyMiddleware(...middlewares)
    );

    store.subscribe(throttle(() => saveState(getPersistedState(store.getState())), 1000));

    return store;
};


export default configureStore;
