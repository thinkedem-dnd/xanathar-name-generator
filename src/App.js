import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { Provider } from 'react-redux';
import Root from './components/Root';
import ReactGA from 'react-ga';

class App extends Component {
    constructor() {
        super(...arguments);
        this.state = {
            initialised: (!!process.env.REACT_APP_GA_CODE && ReactGA.initialize(process.env.REACT_APP_GA_CODE))
        };
    }

    componentDidMount() {
        if (this.state.initialised) {
            ReactGA.pageview(window.location.pathname + window.location.search);
        }
    }

    render() {
        const {store} = this.props;
        return (
            <MuiThemeProvider>
                <Provider store={store}>
                    <Root/>
                </Provider>
            </MuiThemeProvider>
        );
    }

}
export default App;
