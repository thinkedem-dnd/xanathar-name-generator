import { combineReducers } from 'redux';
import { ActionTypes } from '../constants';

const nameIds = (state = [], action) => {
    switch (action.type) {
        case ActionTypes.NAME_ADD:
            return [action.response.result, ...state];

        case ActionTypes.NAME_REMOVE:
            return state.filter(id => id !== action.data.id);

        case ActionTypes.NAME_CLEAR:
            return [];

        default:
            return state;
    }
};

const names = (state = {}, action) => {
    if (action.response) {
        return {
            ...state,
            ...action.response.entities.name
        };
    }
    switch (action.type) {
        case ActionTypes.NAME_REMOVE:
            return {
                ...state,
                [action.data.id]: undefined
            };

        case ActionTypes.NAME_CLEAR:
            return {};

        default:
            return state;
    }
};

const nameList = combineReducers({nameIds, names});

export default nameList;
export const getNames = (state) => state.nameIds.map(id => state.names[id]);
