import { combineReducers } from 'redux';
import { ActionTypes } from '../constants';

const tableIds = (state = [], action) => {
    switch (action.type) {
        case ActionTypes.TABLE_LIST_SUCCESS:
            return action.response.result;

        default:
            return state;
    }
};

const tables = (state = {}, action) => {
    switch (action.type) {
        case ActionTypes.TABLE_LIST_SUCCESS:
            return {
                ...state,
                ...action.response.entities.table
            };

        default:
            return state;
    }
};

const isFetching = (state = false, action) => {
    switch (action.type) {
        case ActionTypes.TABLE_LIST_REQUEST:
            return true;
        case ActionTypes.TABLE_LIST_SUCCESS:
        case ActionTypes.TABLE_LIST_FAILURE:
            return false;
        default:
            return state;
    }
};

const table = combineReducers({tableIds, tables, isFetching});

export default table;

export const getPersistedState = ({tableIds, tables}) => ({tableIds, tables});

export const getIsFetching = (state) => state.isFetching;
export const getTableList = (state) => state.tableIds.map(id => state.tables[id]);
