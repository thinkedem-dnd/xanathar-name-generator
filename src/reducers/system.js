import { combineReducers } from "redux";
import { ActionTypes } from '../constants';

const cardExpanded = (state = {}, action) => {
    switch (action.type) {
        case ActionTypes.TOGGLE_CARD:
            if (!action.card) { return state; }
            return {
                ...state,
                [action.card]: !state[action.card]
            };

        default:
            return state;
    }
};

const system = combineReducers({
    cardExpanded,
});

export default system;
export const getPersistedState = (state) => ({
    ...state
});

export const isCardExpanded = (state, card) => (!!state.cardExpanded[card]);
