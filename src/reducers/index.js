import { combineReducers } from 'redux';
import table, * as fromTable from './table';
import formula, * as fromFormula from './formula';
import format, * as fromFormat from './format';
import names, * as fromNames from './names';
import editor, * as fromEditor from './editor';
import system, * as fromSystem from "./system";

const store = combineReducers({
    table, formula, format, names, editor, system
});

export default store;
export const getPersistedState = ({names, format, system, table, formula}) => ({
    names, format,
    table: fromTable.getPersistedState(table),
    formula: fromFormula.getPersistedState(formula),
    system: fromSystem.getPersistedState(system)
});

export const isFetchingTable = (state) => fromTable.getIsFetching(state.table);
export const isFetchingFormula = (state) => fromFormula.getIsFetching(state.formula);

export const isFetching = (state) => (
    isFetchingTable(state) || isFetchingFormula(state)
);

export const getTableList = (state) => fromTable.getTableList(state.table);

export const getFormulaList = (state) => fromFormula.getFormulaList(state.formula);

export const getFormatName = (state) => fromFormat.getFormatName(state.format);
export const getFormula = (state) => fromFormat.getFormula(state.format);
export const getFormulaElements = (state) => fromFormat.getElements(state.format);

export const getNames = (state) => fromNames.getNames(state.names);

export const isEditing = (state) => fromEditor.getIsEditing(state.editor);
export const getChipDraft = (state) => fromEditor.getDraft(state.editor);
export const getChipValidation = (state) => fromEditor.getValidation(state.editor);

export const isCardExpanded = (state, card) => fromSystem.isCardExpanded(state.system, card);
