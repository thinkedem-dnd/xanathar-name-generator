import { combineReducers } from 'redux';
import { ActionTypes, CUSTOM_FORMULA_ID } from '../constants';

const getDefaultFormula = () => ({
    [CUSTOM_FORMULA_ID]: {id: CUSTOM_FORMULA_ID, name: 'Custom', formula: ''}
});

const formulaIds = (state = [CUSTOM_FORMULA_ID], action) => {
    switch (action.type) {
        case ActionTypes.FORMULA_LIST_SUCCESS:
            return [CUSTOM_FORMULA_ID].concat(action.response.result);

        default:
            return state;
    }
};

const formulas = (state = getDefaultFormula(), action) => {
    switch (action.type) {
        case ActionTypes.FORMULA_LIST_SUCCESS:
            return {
                ...state,
                ...action.response.entities.formula
            };
        default:
            return state;
    }
};

const isFetching = (state = false, action) => {
    switch (action.type) {
        case ActionTypes.FORMULA_LIST_REQUEST:
            return true;
        case ActionTypes.FORMULA_LIST_SUCCESS:
        case ActionTypes.FORMULA_LIST_FAILURE:
            return false;
        default:
            return state;
    }
};

const formula = combineReducers({formulaIds, formulas, isFetching});

export default formula;

export const getPersistedState = ({formulaIds, formulas}) => ({formulaIds, formulas});

export const getIsFetching = (state) => state.isFetching;
export const getFormulaList = (state) => state.formulaIds.map(id => state.formulas[id]);
