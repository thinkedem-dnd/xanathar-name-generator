import { combineReducers } from 'redux';
import { ActionTypes, ElementTypes } from "../constants";

const getElement = () => ({
    id:      null,
    code:    '',
    type:    ElementTypes.CONSTANT,
    table:   null,
    prob:    1
});

const validateDraft = (data = {}) => {
    let validation = {};

    if (!!data.prob) {
        const prob = Number(data.prob);
        if (isNaN(prob) || 0 > prob || 1 < prob) {
            validation.probError = 'Must be a number between 0 and 1';
        }
    }
    if (data.type === ElementTypes.CONSTANT) {
        if (data.code.includes('|')) {
            validation.codeError = "The string cannot contain the | character";
        }
    }
    return validation;
};

const isEditing = (state = false, action) => {
    switch (action.type) {
        case ActionTypes.CHIP_EDIT:
            return !!action.isEditing;
        case ActionTypes.CHIP_SAVE:
            return false;

        default:
            return state;
    }
};

const draft = (state = getElement(), action) => {
    switch (action.type) {
        case ActionTypes.CHIP_EDIT:
            return action.draft || getElement();

        case ActionTypes.CHIP_UPDATE_DRAFT:
            return {
                ...state,
                [action.field]: action.value
            };

        default:
            return state;
    }
};

const editor = combineReducers({draft, isEditing});

export default editor;
export const getIsEditing = (state) => state.isEditing;
export const getDraft = (state) => state.draft;
export const getValidation = (state) => validateDraft(state.draft);
