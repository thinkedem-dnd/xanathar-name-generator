import { ActionTypes, CUSTOM_FORMULA_ID } from '../constants';
import { generateCode, generateFormula } from "../util/parser";
import update from 'immutability-helper';

const getDefaultState = (formula = '') => ({ formula, elements: [] });

const getFormat = ({formula, id = CUSTOM_FORMULA_ID, name = 'Custom', elements = []}) => ({
    id,
    name,
    formula,
    elements
});

const getCustomFormula = (elements) => ({
    id:     CUSTOM_FORMULA_ID,
    name:   'Custom',
    formula: generateFormula(elements),
    elements
});

const updateFormula = (draft, state) => {
    if (draft.prob === '') {
        draft.prob = 1;
    } else {
        draft.prob = Number(draft.prob);
    }
    draft.code = generateCode(draft);

    let command;
    if (draft.id !== null) {
        const formatIndex = state.elements.findIndex(({id}) => id === draft.id);
        command = { [formatIndex]: { $set: draft} };
    } else {
        draft.id = Date.now();
        command = { $push: [draft] };
    }
    return getCustomFormula(update(state.elements, command));
};

const format = (state = getDefaultState(), action) => {
    let index;

    switch (action.type) {

        case ActionTypes.FORMAT_SET:
            return getFormat(action.data);

        case ActionTypes.CHIP_MOVE:
            const chip = state.elements[action.chipIndex];
            return getCustomFormula(update(state.elements, {
                $splice: [
                    [action.chipIndex, 1],
                    [action.afterIndex, 0, chip]
                ]
            }));

        case ActionTypes.CHIP_SAVE:
            return updateFormula(action.draft, state);

        case ActionTypes.CHIP_REMOVE:
            index = state.elements.findIndex(({id}) => id === action.chipId);
            return getCustomFormula(update(state.elements, {
                $splice: [ [index, 1] ]
            }));

        default:
            return state;
    }
};

export default format;
export const getFormatName = (state) => state.name;
export const getFormula = (state) => state.formula;
export const getElements = (state) => state.elements;
